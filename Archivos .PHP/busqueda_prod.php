<?php
include('connection.php');
session_start();

$conexion= ConectarBdd();
$cantidad= null;

if(isset($_POST['agregar']))
{
//	echo '  34 : aqui estoy ';
    if(isset($_SESSION['add_carro']))
    {
        $item_array_id_cart = array_column($_SESSION['add_carro'],'item_id');
        if(!in_array($_GET['id'],$item_array_id_cart))
        {
            $count= count($_SESSION['add_carro']);
            $item_array = array(
                'item_id'        => $_GET['id'],
                'item_nombre'    => $_POST['hidden_nombre'],
                'item_precio'    => $_POST['hidden_precio'],
                'item_cantidad'  => $_POST['cantidad'],
            );
            $_SESSION['add_carro'][$count]=$item_array;
        }
        else
            {
              echo '<script>alert("El Producto ya existe en el carro de compras!");</script>';
            }
    }
    else
        {
            $item_array = array(
                'item_id'        => $_GET['id'],
                'item_nombre'    => $_POST['hidden_nombre'],
                'item_precio'    => $_POST['hidden_precio'],
                'item_cantidad'  => $_POST['cantidad'],
            );
            $_SESSION['add_carro'][0] = $item_array;
        }
}
if(isset($_GET['action']))
{
    if($_GET['action']=='delete')
    {
        foreach ($_SESSION['add_carro'] AS $key => $value)
        {
            if($value['item_id'] == $_GET['id'])
            {
				unset($_SESSION['add_carro'][$key]);
//				unset($_SESSION['add_carro']);
                echo '<script>alert("El Producto fue eliminado del carro de compras!");</script>';
                echo '<script>window.location="busqueda_prod.php";</script>';
            }
        }
    }
}

// se controla la variable $buscar_prod para no perder la busqueda al recargar el formulario. 
if (empty($_REQUEST['buscar_prod'])){
//	$_SESSION['buscar_productos'] = strtoupper($_REQUEST['buscar_prod']);
//	$buscar_prod = $_POST['hidden_nombre'];
//	echo ' linea 78 Nombre FIJO:  '.$_SESSION['buscar_productos'];
}else{
//	$buscar_prod = strtoupper($_REQUEST['buscar_prod']);
	$_SESSION['buscar_productos'] = strtoupper($_REQUEST['buscar_prod']);
//	echo ' linea 81 producto inicial:  '.$_SESSION['buscar_productos'];	
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Electrónica</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.4.1.css" rel="stylesheet">
</head>
<body>  
    <hr>
		<h5 class="text-center"> Búsqueda de Productos </h5>
    <hr>
		<table width="1100" border="1p" align="center">	
			<thead align="center">
				<th width="130" bgcolor="#C5C5C5"><b> Id Producto </b></th>
				<th width="230" bgcolor="#C5C5C5"><b> Producto    </b></th>
				<th width="180" bgcolor="#C5C5C5"><b> Marca       </b></th>
				<th width="400" bgcolor="#C5C5C5"><b> Descripción </b></th>
				<th width="120" bgcolor="#C5C5C5"><b> Precio      </b></th>
				<th width="230" bgcolor="#C5C5C5"><b> Imagen      </b></th>
				<th width="100" bgcolor="#C5C5C5"><b> Cantidad	  </b></th>
				<th width="250" bgcolor="#C5C5C5"><b> Comprar	  </b></th>
			</thead>
			<tbody>
			<?php
//				$sql="SELECT * FROM productos where nombre_producto like '".$_SESSION['buscar_productos']."%'";
				$sql="SELECT * FROM productos where nombre_producto like '".$_SESSION['buscar_productos']."%' and stock_producto >0";
				
				$resul= mysqli_query($conexion,$sql);
				$cantidad_carro = mysqli_num_rows($resul);
				if($cantidad_carro > 0){
					while ($row=mysqli_fetch_array($resul)){
			?>
			<div>
				<form method="post" action="busqueda_prod.php?action=add&id=<?php echo $row['id_producto']; ?>" name="signup-form">
				<tr width="1100">
					<td> <?php echo $row['id_producto']; ?> </td>
					<td> <?php echo $row['nombre_producto']; ?> </td>
					<td> <?php echo $row['marca_producto']; ?> </td>
					<td> <?php echo $row['descripcion_producto']; ?> </td>
					<td align="right"> <?php echo "$"; echo " "; echo number_format($row['precio_producto']); ?>  </td>
					<td align="center"> <?php echo "<img src='".$row['imagen_producto']."' width='100'>"; ?> </td>
  
                    <input type="hidden" name="hidden_nombre" value="<?php echo $row['nombre_producto'];?>" />
                    <input type="hidden" name="hidden_precio" value="<?php echo $row['precio_producto'];?>" />
					
					<td align="center"> <input type="text" name="cantidad" value="1" />  </td>

					<td align="center"> <input type="submit" name="agregar" style="margin-top:1px;" class="btn btn-success" value="Agregar al carro" /> </td>
				</tr>
				</form>
			</div>
			<?php
				}
			}
			?>	
			</tbody>
		</table>
		<hr>
		<table width="950" border="0" align="center">
			<tbody>
				<tr>
					<td>
					<?php  
						if ($cantidad_carro == 0) {
							echo '<tr><td> <h4 class="text-center"> Lo sentimos, no tenemos los productos que buscas </h4> </tr></td>';
						}
					?>
					</td>
				</tr>
			</tbody>
		</table>
<!-- esta es el área para controlar el detalle de la orden compra  -->
		<hr>
		<table width="950" border="1" align="center">
			<tbody>
				<tr>
					<td align="center" bgcolor="#C5C5C5"><b> Detalle de la Orden de Compra </b>
					</td>
				</tr>
			</tbody>
		</table>
		<table width="950" border="1" align="center">
			<tbody>
				<tr align="center">
					<th width="20%">Id Producto</th>
					<th width="20%">Producto</th>
					<th width="15%">Precio</th>
					<th width="15%">Cantidad</th>
					<th width="10%">Total</th>
					<th width="20">Action</th>
				</tr>

			<?php
			if(!empty($_SESSION["add_carro"])) 
			{
				$total = 0;
				foreach($_SESSION["add_carro"] as $keys => $values)
				{
				?>
				<tr>
					<td width="20%"><?php echo $values["item_id"]; ?></td>
					<td width="20%"><?php echo $values["item_nombre"]; ?></td>
					<td width="15%" align="right"><?php echo "$"; echo " "; echo number_format($values["item_precio"], 0); ?></td>
					<td width="15%" align="right"><?php echo $values["item_cantidad"]; ?></td>
					<td width="10%" align="right"><?php echo "$"; echo " "; echo number_format($values["item_cantidad"] * $values["item_precio"], 0);?></td>
					<td width="20%" align="center"><a href="busqueda_prod.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Eliminar del carro</span></a></td>
				</tr>
				<?php
				$total = $total + ($values["item_cantidad"] * $values["item_precio"]);
				$_SESSION["total_venta"] = $total;
				}
				?>
				<td colspan="6" align="right" style="color: white"><b> .</b></td>
				<tr>
                    <td colspan="4" align="right" style="color: blue"><b>Total compra: </b></td>
                    <td align="right" style="color: blue"><b> $ <?php echo number_format($total, 0); ?> </b></td>
                    <td align="center" style="color: blue"><a href="pago.php?>"><span><b>Realizar compra</b></span></a></td>
                </tr>
				<?php
            }else{
                ?>
				<tr>
				<!--	<td colspan="4" style="color: red" align="center"><strong>No hay Producto Agregado!</strong></td> -->
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>	
	</br>
	<table width="800" border="0" align="center">
		<tbody>
			<tr>
				<td width="680" align="right">&nbsp;
				</td>
				<td width="120" align="right">
					<h6 class="card-title"> <a class="nav-link" href="inicio.php" target="principal" > ir al home </a> </h6>
				</td>
			</tr>	 
		</tbody>
	</table>
<!--	</form>-->
    <div class="container text-white bg-dark p-4">
		<div class="row">
			<div class="col-6 col-md-8 col-lg-7">
			</div> 
			<div class="col-md-4 col-lg-5 col-6">
				<address>
					<strong> CASA MATRIZ SANTIAGO, Inc.</strong><br>
					Avenida Principal #1236<br>
					Providencia, Santiago de Chile<br>
					<abbr title="Phone">Teléfono</abbr> (+56)2.2422.5500
				</address>
				<address>
					<strong>Correo Electrónico</strong><br>
					<a href="mailto:#">contacto@electronica.cl</a>
				</address>
			</div>
		</div>
    </div>
    <footer class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>Copyright © Adolfo. Todos los derechos reservados.</p>
				</div>
			</div>
		</div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.4.1.js"></script>
 </body>
</html>
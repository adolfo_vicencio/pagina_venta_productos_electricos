<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Electrónica</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.4.1.css" rel="stylesheet">
</head>
<body>  
<form method="post" action="" name="signup-form">
    <hr>
		<h2 class="text-center">----------   Productos   ----------</h2>
    <hr>

	<table width="643" border="0" align="center">
		<tbody>
			<tr>
				<td width="637" align="center">
					<h6 class="card-title"> Listado de productos</h6>

				</td>
			</tr>
		</tbody>
	</table>
</form>
	<hr>

    <div class="container text-white bg-dark p-4">
		<div class="row">
			<div class="col-6 col-md-8 col-lg-7">
				<!--<div class="row text-center">
					<div class="col-sm-6 col-md-4 col-lg-4 col-12">
						<ul class="list-unstyled">
							<li class="btn-link"> <a>Link anchor</a> </li>
							<li class="btn-link"> <a>Link anchor</a> </li>
						</ul>
					</div>
				</div>-->
			</div> 
			<div class="col-md-4 col-lg-5 col-6">
				<address>
					<strong> CASA MATRIZ SANTIAGO, Inc.</strong><br>
					Avenida Principal #1236<br>
					Providencia, Santiago de Chile<br>
					<abbr title="Phone">Teléfono</abbr> (+56)2.2422.5500
				</address>
				<address>
					<strong>Correo Electrónico</strong><br>
					<a href="mailto:#">contacto@electronica.cl</a>
				</address>
			</div>
		</div>
    </div>
    <footer class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>Copyright © Adolfo. Todos los derechos reservados.</p>
				</div>
			</div>
		</div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.4.1.js"></script>
 </body>
</html>
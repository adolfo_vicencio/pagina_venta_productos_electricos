<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Electrónica</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.4.1.css" rel="stylesheet">
</head>
<body>  
		<!-- <h2 class="text-center">----------   Ventas   ----------</h2> -->
    <hr>
		<div class="container">
			<div class="row text-center">
				<div class="col-md-4 pb-1 pb-md-0">
					<div class="card">
						<img src="images/oficina/equipoventas.jpg" alt="Card image cap" width="50" height="190" class="card-img-top">
						<div class="card-body">
						  <h5 class="card-title">Ventas Telefónicas</h5>
				 			<table width="200" align="left">
								<tbody>
									<tr>
										<td><p class="card-text1"> Teléfono  +56.2.2422.1232 </p> </td>
									</tr>
									<tr>
										<td><p class="card-text1"> Teléfono  +56.2.2422.1233 </p> </td>
									</tr>
									<tr>
										<td><p class="card-text1"> Teléfono  +56.2.2422.1234 </p> </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-4 pb-1 pb-md-0">
					<div class="card">
						<img src="images/oficina/bodega.jpg" alt="Card image cap" width="50" height="190" class="card-img-top">
						<div class="card-body">
						  <h5 class="card-title">Bodega</h5>
				 			<table width="200" align="left">
								<tbody>
									<tr>
										<td><p class="card-text1"> Teléfono  +56.2.2422.2455 </p> </br> </br> </br> </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-4 pb-1 pb-md-0">
					<div class="card">
						<img class="card-img-top" src="images/oficina/secretaria.jpg" width="80" height="200" alt="Card image cap">
						<div class="card-body">
							<h5 class="card-title">Secretaria Ejecutiva</h5>
				 			<table width="200" align="left">
								<tbody>
									<tr>
										<td><p class="card-text1"> Teléfono  +56.2.2422.3324 </p> </br> </br> </br> </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<table width="800" border="0" align="center">
				<tbody>
					<tr>
						<td width="680" align="right">&nbsp;
						</td>
						<td width="120" align="right">
							<h6 class="card-title"> <a class="nav-link" href="inicio.php" target="principal" > ir al home </a> </h6>
						</td>
					</tr>	 
				</tbody>
			</table>
		</div>
		<hr>
    <div class="container text-white bg-dark p-4">
		<div class="row">
			<div class="col-6 col-md-8 col-lg-7">
			</div> 
			<div class="col-md-4 col-lg-5 col-6">
				<address>
					<strong> CASA MATRIZ SANTIAGO, Inc.</strong><br>
					Avenida Principal #1236<br>
					Providencia, Santiago de Chile<br>
					<abbr title="Phone">Teléfono</abbr> (+56)2.2422.5500
				</address>
				<address>
					<strong>Correo Electrónico</strong><br>
					<a href="mailto:#">contacto@electronica.cl</a>
				</address>
			</div>
		</div>
    </div>
    <footer class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>Copyright © Adolfo. Todos los derechos reservados.</p>
				</div>
			</div>
		</div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.4.1.js"></script>
 </body>
</html>